package View;

import Model.Pokemon;
import Model.ReadCSV;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.ArrayList;

public class InterfaceTrainerPokemon extends JFrame implements ActionListener {


    static JPanel J1, J2, J3, J4;
    static JLabel L1, L2, L3, L4, L5, L6, L7, L8, L9;
    static JTextField tf1, tf2, tf3, tf4, tf5, tf6, tf7, tf8;
    static JButton Cadastrar, Sair, buscarPokemon;
    static JMenuBar barraDeOpcoes;
    static JMenu op, ajuda;
    static JMenuItem editar, buscar, excluir, fim, sobre, listar, quantidade, buscarType;
    private Container janela;
    static Trainer t;
    static ManageTrainer registro;
    static DataReport report;

    public InterfaceTrainerPokemon() {

        setTitle("Cadastro de Treinadores");
        setBounds(150, 150, 400, 400);
        setResizable(false);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
            }
        });

        registro = new ManageTrainer();

        barraDeOpcoes = new JMenuBar();
        setJMenuBar(barraDeOpcoes);

        op = new JMenu("Opções");
        editar = new JMenuItem("Editar");
        op.add(editar);
        editar.setEnabled(false);
        buscar = new JMenuItem("Buscar treinador");
        buscar.addActionListener(this);
        op.add(buscar);
        buscar.setEnabled(true);
        excluir = new JMenuItem("Excluir treinador");
        excluir.addActionListener(this);
        op.add(excluir);
        excluir.setEnabled(true);
        listar = new JMenuItem("Listar treinadores");
        listar.addActionListener(this);
        op.add(listar);
        listar.setEnabled(true);
        quantidade = new JMenuItem("Quantidade de treinadores");
        quantidade.addActionListener(this);
        op.add(quantidade);
        quantidade.setEnabled(true);
        buscarType = new JMenuItem("Buscar por tipo");
        buscarType.addActionListener(this);
        op.add(buscarType);
        buscarType.setEnabled(true);
        fim = new JMenuItem("Sair");
        fim.addActionListener(this);
        op.add(fim);
        barraDeOpcoes.add(op);
        ajuda = new JMenu("Ajuda");
        sobre = new JMenuItem("Sobre");
        sobre.addActionListener(this);
        ajuda.add(sobre);
        barraDeOpcoes.add(ajuda);

        J1 = new JPanel();
        J1.setLayout(new GridLayout(10, 1, 10, 10));
        J2 = new JPanel();
        J2.setLayout(new GridLayout(10, 1, 10, 10));
        J3 = new JPanel();
        J3.setLayout(new FlowLayout());
        J4 = new JPanel();
        J4.setLayout(new FlowLayout());

        L1 = new JLabel("Nome: ");
        tf1 = new JTextField(15);
        L2 = new JLabel("Login: ");
        tf2 = new JTextField(15);
        L3 = new JLabel("Senha: ");
        tf3 = new JTextField(15);
        L4 = new JLabel("Pokemon 1: ");
        tf4 = new JTextField(15);
        L5 = new JLabel("Pokemon 2: ");
        tf5 = new JTextField(15);
        L6 = new JLabel("Pokemon 3: ");
        tf6 = new JTextField(15);
        L7 = new JLabel("Pokemon 4: ");
        tf7 = new JTextField(15);
        L8 = new JLabel("Pokemon 5: ");
        tf8 = new JTextField(15);

        Cadastrar = new JButton("Cadastrar");
        Cadastrar.addActionListener(this);
        Sair = new JButton("Sair");
        Sair.addActionListener(this);
        buscarPokemon = new JButton("Buscar Pokemon");
        buscarPokemon.addActionListener(this);

        J1.add(L1);
        J1.add(L2);
        J1.add(L3);
        J1.add(L4);
        J1.add(L5);
        J1.add(L6);
        J1.add(L7);
        J1.add(L8);

        J2.add(tf1);
        J2.add(tf2);
        J2.add(tf3);
        J2.add(tf4);
        J2.add(tf5);
        J2.add(tf6);
        J2.add(tf7);
        J2.add(tf8);

        J3.add(Cadastrar);
        J3.add(Sair);
        J3.add(buscarPokemon);

        janela = getContentPane();
        janela.add(J1, BorderLayout.WEST);
        janela.add(J2, BorderLayout.CENTER);
        janela.add(J3, BorderLayout.SOUTH);
        janela.add(J4, BorderLayout.NORTH);
    }




    public static void main(String[] args) {
        InterfaceTrainerPokemon window = new InterfaceTrainerPokemon();
        window.setVisible(true);
    }

    private String nome = new String();
    private String login = new String();
    private String senha = new String();
    private String pokemon1 = new String();
    private String pokemon2 = new String();
    private String pokemon3 = new String();
    private String pokemon4 = new String();
    private String pokemon5 = new String();

    @Override
    public void actionPerformed(ActionEvent e) {

        Object src = e.getSource();

        if(src == Cadastrar) {

            nome = tf1.getText();
            login = tf2.getText();
            senha = tf3.getText();
            pokemon1 = tf4.getText();
            pokemon2 = tf5.getText();
            pokemon3 = tf6.getText();
            pokemon4 = tf7.getText();
            pokemon5 = tf8.getText();

            if(nome.length() == 0 || login.length() == 0 || senha.length() == 0 ||
                    pokemon1.length() == 0 || pokemon2.length() == 0 || pokemon3.length() == 0 ||
                    pokemon4.length() == 0 || pokemon5.length() == 0) {
                JOptionPane.showMessageDialog(null, "Há campo em branco que precisa ser preenchido!", "Atenção", JOptionPane.WARNING_MESSAGE);
            }

            else {
                t = new Trainer();
                t.setNome(nome);
                t.setLogin(login);
                t.setSenha(senha);
                t.setNomePokemon1(pokemon1);
                t.setNomePokemon2(pokemon2);
                t.setNomePokemon3(pokemon3);
                t.setNomePokemon4(pokemon4);
                t.setNomePokemon5(pokemon5);

                registro.inserirTrainer(t);

                //Limpar os campos para o novo cadastro:

                tf1.setText("");
                tf2.setText("");
                tf3.setText("");
                tf4.setText("");
                tf5.setText("");
                tf6.setText("");
                tf7.setText("");
                tf8.setText("");
            }
        }

        if(src == Sair || src == fim) {

            dispose();
            setVisible(false);
        }

        if(src == sobre) {

            Creditos autor = new Creditos();
            autor.setVisible(true);
        }

        if(src == buscar) {

            String nome2 = new String();
            nome2 = JOptionPane.showInputDialog(null, "Estou procurando por(Digite o nome do treinador igual no cadastro): ", "Pesquisa", JOptionPane.QUESTION_MESSAGE);
            t = registro.buscarTrainer(nome2);
            if(t != null) {

                DataReport show = new DataReport();
                show.showTrainer(t);
                show.setVisible(true);
            }
            else {
                JOptionPane.showMessageDialog(null, "Treinador inexistente!");
            }
        }

        if(src == excluir) {

            String indice;
            int id;
            indice = JOptionPane.showInputDialog(null, "Digite o índice do array do Treinador: ", "Excluir", JOptionPane.QUESTION_MESSAGE);
            id = Integer.parseInt(indice);
            registro.removerTrainer(id);
        }

        if(src == listar) {

            int i;
            if(registro.quantTreinadores() == 0) {
                JOptionPane.showMessageDialog(null, "Lista de treinadores vazia!", "Atenção", JOptionPane.WARNING_MESSAGE);
            }
            else {
                for(i = 0; i < registro.quantTreinadores(); ++i) {
                    JOptionPane.showMessageDialog(null, "Nome: " + registro.getTrainer(i).getNome() +
                            "\nLogin: " + registro.getTrainer(i).getLogin() + "\nSenha: " + registro.getTrainer(i).getSenha() +
                            "\nPokemon 1: " + registro.getTrainer(i).getNomePokemon1() + "\nPokemon 2: " + registro.getTrainer(i).getNomePokemon2() +
                            "\nPokemon 3: " + registro.getTrainer(i).getNomePokemon3() + "\nPokemon 4: " + registro.getTrainer(i).getNomePokemon4() + "\nPokemon 5: " + registro.getTrainer(i).getNomePokemon5());
                }
            }
        }

        if(src == quantidade) {
            JOptionPane.showMessageDialog(null, "A lista possui " + registro.quantTreinadores() + " treinador(es) cadastrado(s)!");
        }

        boolean searchByName = false;
        boolean searchByType = false;

        if(src == buscarPokemon) {

            ArrayList<Pokemon> Pokemons = new ArrayList<>();
            try {
                ReadCSV.reader(Pokemons);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            int aux = 1;

            do {
                String buscaNome = JOptionPane.showInputDialog("Insira o nome do Pokemon: ");
                searchByName = ManagePokemon.searchPokemonByName(Pokemons, buscaNome);

            } while(searchByName);
        }

        if(src == buscarType) {
            ArrayList<Pokemon> pokemons = new ArrayList<>();

            try {
                ReadCSV.reader(pokemons);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            int aux = 1;

            do {
                String buscaType = JOptionPane.showInputDialog("Insira o tipo: ");
                searchByType = ManagePokemon.searchPokemonByType(pokemons, buscaType);

            } while(searchByType);
        }
    }
}