package Model;

public class Pokemon {

    private int id;
    private String nome;
    private String type1;
    private String type2;
    private int total;
    private int hp;
    private int attack;
    private int defense;
    private int specialAttack;
    private int specialDefense;
    private int speed;
    private int generation;
    private String legendary;
    private int experience;
    private int height;
    private int weight;
    private String abilitie1;
    private String abilitie2;
    private String abilitie3;
    private String move1;
    private String move2;
    private String move3;
    private String move4;
    private String move5;
    private String move6;
    private String move7;

    public Pokemon(int id, String nome, String type1, String type2, int total, int hp, int attack, int defense, int specialAttack, int specialDefense, int speed, int generation, String legendary, int experience, int height, int weight, String abilitie1, String abilitie2, String abilitie3, String move1, String move2, String move3, String move4, String move5, String move6, String move7) {
        this.id = id;
        this.nome = nome;
        this.type1 = type1;
        this.type2 = type2;
        this.total = total;
        this.hp = hp;
        this.attack = attack;
        this.defense = defense;
        this.specialAttack = specialAttack;
        this.specialDefense = specialDefense;
        this.speed = speed;
        this.generation = generation;
        this.legendary = legendary;
        this.experience = experience;
        this.height = height;
        this.weight = weight;
        this.abilitie1 = abilitie1;
        this.abilitie2 = abilitie2;
        this.abilitie3 = abilitie3;
        this.move1 = move1;
        this.move2 = move2;
        this.move3 = move3;
        this.move4 = move4;
        this.move5 = move5;
        this.move6 = move6;
        this.move7 = move7;
    }

    public Pokemon() {
        setId(0);
        setNome("");
        setType1("");
        setType2("");
        setTotal(0);
        setHp(0);
        setAttack(0);
        setDefense(0);
        setSpecialAttack(0);
        setSpecialDefense(0);
        setSpeed(0);
        setLegendary("");
        setExperience(0);
        setHeight(0);
        setWeight(0);
        setAbilitie1("");
        setAbilitie2("");
        setAbilitie3("");
        setMove1("");
        setMove2("");
        setMove3("");
        setMove4("");
        setMove5("");
        setMove6("");
        setMove7("");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getType1() {
        return type1;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public String getType2() {
        return type2;
    }

    public void setType2(String type2) {
        this.type2 = type2;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getSpecialAttack() {
        return specialAttack;
    }

    public void setSpecialAttack(int specialAttack) {
        this.specialAttack = specialAttack;
    }

    public int getSpecialDefense() {
        return specialDefense;
    }

    public void setSpecialDefense(int specialDefense) {
        this.specialDefense = specialDefense;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }

    public String getLegendary() {
        return legendary;
    }

    public void setLegendary(String legendary) {
        this.legendary = legendary;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getAbilitie1() {
        return abilitie1;
    }

    public void setAbilitie1(String abilitie1) {
        this.abilitie1 = abilitie1;
    }

    public String getAbilitie2() {
        return abilitie2;
    }

    public void setAbilitie2(String abilitie2) {
        this.abilitie2 = abilitie2;
    }

    public String getAbilitie3() {
        return abilitie3;
    }

    public void setAbilitie3(String abilitie3) {
        this.abilitie3 = abilitie3;
    }

    public String getMove1() {
        return move1;
    }

    public void setMove1(String move1) {
        this.move1 = move1;
    }

    public String getMove2() {
        return move2;
    }

    public void setMove2(String move2) {
        this.move2 = move2;
    }

    public String getMove3() {
        return move3;
    }

    public void setMove3(String move3) {
        this.move3 = move3;
    }

    public String getMove4() {
        return move4;
    }

    public void setMove4(String move4) {
        this.move4 = move4;
    }

    public String getMove5() {
        return move5;
    }

    public void setMove5(String move5) {
        this.move5 = move5;
    }

    public String getMove6() {
        return move6;
    }

    public void setMove6(String move6) {
        this.move6 = move6;
    }

    public String getMove7() {
        return move7;
    }

    public void setMove7(String move7) {
        this.move7 = move7;
    }
}
